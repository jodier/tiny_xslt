Installation
============

	pip install tiny_xslt

Usage
=====

	import tiny_xslt

	result = tiny_xslt.transform('file.xsl', 'file.xml')
